#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

mkdir ../src/DLL

while read file; do
  cp $file ../src/DLL/
done < ldd-list.txt
