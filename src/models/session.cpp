#include "session.h"

Session::Session() {
  _physics = std::make_shared<Physics>();
}

Session::~Session() {
  _scene->destroy_scene();
}

void
Session::start() {
  if (!_started) {
    _player->clear_counters();
    _started = true;
    _finished = false;
    show_overlays();
  }

  _ball.start();
}

bool
Session::has_finished() {
  return _finished;
}

void
Session::initialize(Scene::shared scene, Sound::shared sound,
      GUI::shared gui, Player::shared player, Animation::shared animation) {

  _scene = scene;
  _sound = sound;
  _gui = gui;
  _player = player;
  _animation = animation;

  _started = false;
  _finished = false;

  create_background();

  create_limits();
  initialize_bricks();
  initialize_paddle();
  initialize_ball();

  initialize_overlays();

  add_collision_hooks();
}

void
Session::create_limits() {
  Ogre::SceneNode* node = _scene->get_node("limit");
  _scene->get_child_node("", "limit");

  _physics->create_rigid_body(_physics->create_shape(btVector3(-1, 0, 0), 0),
                              btVector3(16, 0, 0), node);
  _physics->create_rigid_body(_physics->create_shape(btVector3(1, 0, 0), 0),
                              btVector3(-8, 0, 0), node);

  _physics->create_rigid_body(_physics->create_shape(btVector3(0, -1, 0), 0),
                              btVector3(0, 4.5, 0), node);

  btCollisionShape* drain_box_shape =  _physics->create_shape(btVector3(40, 1, 1));
  _drain = _physics->create_rigid_body(drain_box_shape,  btVector3(0, -11, 0), node);
}

void
Session::create_background() {
  _scene->create_plane("Z", "ground", "plane_ground", "", "water");
}

void
Session::update(float deltaT) {
  _physics->step_simulation(deltaT, 1);

  if (!_ball._started)
    reset_ball();

  _ball.update();

  _physics->check_collision();
  _animation->update(deltaT);
  _player->increase_time(deltaT);

  update_overlays();


  if(_player->has_died() || _player->all_bricks_deleted())
    _finished = true;
}

void
Session::update_overlays(){
  if (!_started)
    return;

  _timer_overlay->
    setCaption("Time: " + to_string_with_precision(_player->get_time_counter(), 3));

  _lifes_overlay->setCaption("Lifes: " + std::to_string(_player->get_lifes_number()));

  _score_overlay->setCaption("Score: " + std::to_string(_player->get_score()));
}

void
Session::clean_session() {
  _scene->destroy_node("ground");
  reset_overlays();
}

void
Session::reset_overlays(){
  _timer_overlay->hide();
  _lifes_overlay->hide();
  _score_overlay->hide();

  _timer_overlay->setCaption("");
  _lifes_overlay->setCaption("");
  _score_overlay->setCaption("");
}

void
Session::show_overlays() {
  if(!_started)
    return;

  _timer_overlay->show();
  _lifes_overlay->show();
  _score_overlay->show();
}

std::string
Session::to_string_with_precision(const float number, const int n) {
    std::ostringstream number_stringify;
    number_stringify << std::setprecision(n) << number;
    return number_stringify.str();
}

void
Session::initialize_overlays() {
  _timer_overlay = _gui->create_overlay("Info", "Timer");
  _lifes_overlay = _gui->create_overlay("Info", "Lifes");
  _score_overlay = _gui->create_overlay("Info", "Score");
}

void
Session::initialize_bricks() {
  _board = Session::Bricks(_player->get_level() * 6);
  int index = 0;

  for(auto& brick: _board){
    create_brick(brick, index++);
  }
}

void
Session::create_brick(Brick& brick, int index) {
  float starting_pos_x = -6.5;
  float starting_pos_y = -2;

  float offset_x = 2.1, offset_y = 1.1;
  float pos_x = offset_x * get_row(index) + starting_pos_x;
  float pos_y = offset_y * get_column(index) + starting_pos_y;

  std::stringstream name;
  name << "brick" << index;
  Ogre::Entity* entity = _scene->create_entity(name.str(), "mountain.mesh");
  Ogre::SceneNode* node = _scene->create_graphic_element(entity, "", name.str());

  btCollisionShape* shape = _physics->create_shape(
        new MeshStrider(entity->getMesh().get()));

  btVector3 origin(pos_x, pos_y, 0);
  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btTransform transformation(rotation, origin);

  btRigidBody* body = _physics->create_rigid_body(transformation, node, shape, 100000);

  brick.initialize(body, node, _scene, _physics, _player, _animation);
}

void
Session::initialize_ball() {
  Ogre::Entity* entity = _scene->create_entity("ball_ent", "Sphere.mesh");
  Ogre::SceneNode* node = _scene->create_graphic_element(entity, "", "ball");

  node->setScale(Ogre::Vector3(0.1, 0.1, 0.1));
  btCollisionShape* shape = _physics->create_shape(0.1);

  btVector3 origin = _paddle._body->getCenterOfMassPosition() + btVector3(0, 2, 0);

  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btTransform transformation(rotation, origin);

  btRigidBody* body = _physics->create_rigid_body(transformation, node, shape, 1);
  body->setRestitution(1.5);
  body->setLinearFactor(btVector3(1, 1, 0));

  _ball.initialize(body, _sound, _scene, _physics);

  _particle = _scene->get_particle(_scene->get_child_node(node, "ball_particle"),
                          "ball_particle", "Bomb/ProjSmoke");
}

void
Session::reset_ball() {
  btVector3 offset = btVector3(0, 2, 0);
  btVector3 position = _paddle.get_position() + offset;
  _ball.reset(position);
}

void
Session::initialize_paddle() {
  Ogre::Entity* entity = _scene->create_entity("paddle_ent", "paddle.mesh");
  Ogre::SceneNode* node = _scene->create_graphic_element(entity, "", "paddle");

  btCollisionShape* shape = _physics->create_shape(btVector3(1.8, 0.5, 1));
  btVector3 origin(3, -9, 0);
  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btTransform transformation(rotation, origin);

  btRigidBody* body = _physics->create_rigid_body(transformation, node, shape, 100000);
  body->setLinearFactor(btVector3(1, 1, 0));
  body->setAngularFactor(btVector3(0, 0, 0));

  _paddle.initialize(body);
}

int
Session::get_row(int index) {
  return index % _player->get_level();
}

int
Session::get_column(int index) {
  return index / _player->get_level();
}

void
Session::move(std::string node_name, btVector3 increment) {
  _paddle.move(btVector3(increment));
}

void
Session::add_collision_hooks() {
  for(auto& brick: _board) {
    _physics->add_collision_hooks(Physics::CollisionPair{_ball._body, brick._body},
                                std::bind(&Ball::collide_with_brick, _ball));

    _physics->add_collision_hooks(Physics::CollisionPair{brick._body, _ball._body},
                                std::bind(&Brick::collision, brick));
  }

  _physics->add_collision_hooks(Physics::CollisionPair{_paddle._body, _ball._body},
                                std::bind(&Ball::collide_with_paddle, _ball));

  _physics->add_collision_hooks(Physics::CollisionPair{_ball._body, _drain},
                               std::bind(&Player::losing_life, _player));

  _physics->add_collision_hooks(Physics::CollisionPair{_drain, _ball._body},
                               std::bind(&Session::reset_ball, this));
}
