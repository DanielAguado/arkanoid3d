#ifndef SESSION_HPP
#define SESSION_HPP
#include <memory>
#include <sstream>

#include "scene.h"
#include "gui.h"
#include "meshstrider.h"

#include "player.h"
#include "brick.h"
#include "ball.h"
#include "paddle.h"

class Session
{
public:
  typedef std::shared_ptr<Session> shared;

  Session();
  ~Session();

  void initialize(Scene::shared scene, Sound::shared sound,
      GUI::shared gui, Player::shared player, Animation::shared animation);
  void move(std::string node_name, btVector3 increment);
  void start();
  void update(float deltaT);
  void update_overlays();
  void show_overlays();

  bool has_finished();
  void clean_session();
  void reset_overlays();

private:
  typedef std::vector<Brick> Bricks;

  bool _started;
  bool _finished;

  Bricks _board;
  Ball _ball;
  Paddle _paddle;

  Player::shared _player;

  Physics::shared _physics;
  Scene::shared _scene;
  Sound::shared _sound;
  GUI::shared _gui;
  Animation::shared _animation;

  Ogre::OverlayElement* _timer_overlay;
  Ogre::OverlayElement* _lifes_overlay;
  Ogre::OverlayElement* _score_overlay;

  btRigidBody* _drain;
  Ogre::ParticleSystem* _particle;

  void create_background();

  void create_board();
  void create_limits();
  void initialize_bricks();
  void create_brick(Brick& brick, int index);
  void initialize_paddle();
  void initialize_ball();
  void reset_ball();

  void initialize_overlays();

  void add_collision_hooks();

  int get_row(int index);
  int get_column(int index);

  std::string to_string_with_precision(const float number, const int n);
};


#endif
