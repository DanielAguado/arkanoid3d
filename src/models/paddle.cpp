#include "paddle.h"

Paddle::Paddle(){

}

Paddle::~Paddle(){

}

void
Paddle::initialize(btRigidBody* body){
  _body = body;
}

void
Paddle::move(btVector3 direction) {
  _body->setLinearVelocity(direction);
}

btVector3
Paddle::get_position() {
  return _body->getCenterOfMassPosition();
}
