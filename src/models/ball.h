#ifndef BALL_
#define BALL_HPP
#include <memory>
#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include <cmath>

/* #include <cstdlib> */
/* #include <ctime> */

#include "physics.h"
#include "sound.h"
#include "scene.h"

class Ball
{
public:
  Ball();
  ~Ball();

  void initialize(btRigidBody* body, Sound::shared sound,
                  Scene::shared scene, Physics::shared physics);
  void start();
  void reset(btVector3 position);
  void move(btVector3 direction);
  void update();
  void collide_with_brick();
  void collide_with_paddle();

  btVector3 get_direction();

  btRigidBody* _body;
  bool _started;

private:
  float correct_direction(float direction, float correction);

  Sound::shared _sound;
  Scene::shared _scene;
  Physics::shared _physics;
};


#endif
