#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <sstream>
#include <memory>
#include <iomanip>

class Player {
public:
  typedef std::shared_ptr<Player> shared;

  Player(int level);
  ~Player();

  void reset();
  void initialize(int level);

  bool all_bricks_deleted();
  bool has_died();

  int get_number_of_deleted_bricks();
  int get_level();
  int get_score();

  void clear_counters();
  float get_time_counter();

  void increase_time(float deltaT);
  void increase_score();

  void losing_life();
  int get_lifes_number();

  std::string to_string();

 private:
  bool _losing_life;
  float _counter, _lose_delay, _next_lose;

  int _level;
  int _score;
  float _time_counter;
  int _lifes;

  int _deleted_bricks;
  std::string to_string_with_precision(const float number, const int n);
};

#endif
