#include "player.h"
#include <iostream>

Player::Player(int level) {
  _level = level;
  reset();
}

Player::~Player(){

}

void
Player::reset(){
  _score = _deleted_bricks = 0;
  _counter = _time_counter = 0.0;
  _lifes = 3;
  _next_lose = 1.f;
}

int
Player::get_level(){
	return _level;
}

int
Player::get_score(){
	return _score;
}

void
Player::clear_counters(){
	_time_counter = 0.0;
	_score = 0;
}

float
Player::get_time_counter(){
	return _time_counter;
}

int
Player::get_number_of_deleted_bricks() {
  return _deleted_bricks;
}

bool
Player::has_died(){
  return _lifes <= 0;
}

bool
Player::all_bricks_deleted(){
  return _deleted_bricks >= (_level * 6);
}

void
Player::increase_time(float deltaT){
	_time_counter += deltaT;
  _counter += deltaT;

  if(_counter >= _next_lose )
    _losing_life = true;
}

void
Player::increase_score(){
	_score++;
  _deleted_bricks++;
}

void
Player::losing_life() {
  if(_losing_life)   {
    _losing_life = false;
    _counter = 0;
    _lifes--;
  }
}

int
Player::get_lifes_number(){
	return _lifes;
}

std::string
Player::to_string() {
  std::stringstream string;
  string << "Time: " << to_string_with_precision(_time_counter, 4)
         << " Score: " << _score
         << " Lifes: " << _lifes << "\n";

  return string.str();

}

std::string
Player::to_string_with_precision(const float number, const int n) {
    std::ostringstream number_stringify;
    number_stringify << std::setprecision(n) << number;
    return number_stringify.str();
}
