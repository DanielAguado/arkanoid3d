#include "brick.h"

Brick::Brick(){
}

Brick::~Brick(){
}

void
Brick::initialize(btRigidBody* body, Ogre::SceneNode* node, Scene::shared scene,
				Physics::shared physics, Player::shared player, Animation::shared animation){

  _body = body;
  _physics = physics;
  _scene = scene;
  _node = node;
  _player = player;
  _animation = animation;
}

void
Brick::collision() {
  _animation->activate(
             static_cast<Ogre::Entity*>(_node->getAttachedObject(0)),
             "deleting", std::bind(&Brick::remove_from_scene, this));

  Ogre::Vector3 position = _scene->convert_btvector3_to_vector3(_body->getCenterOfMassPosition());
  _scene->get_particle(_node->getName() + "particle", "Bomb/Explosion", position);
}

void
Brick::remove_from_scene() {
  _physics->remove_rigid_body(_body);
  _scene->destroy_node(_node);
  _player->increase_score();
}
