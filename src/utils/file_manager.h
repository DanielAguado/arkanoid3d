#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H
#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <vector>

class FileManager {
 public:
  typedef std::vector<std::string> Match;
  typedef std::vector<Match> Results;

  FileManager();
  virtual ~FileManager();

  std::vector<std::string> get_content(std::string file);
  void append(std::string file_name, std::string line);
  void overwrite(std::string file_name, std::vector<std::string> content);
};
#endif
