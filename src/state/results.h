#ifndef RESULTS_H
#define RESULTS_H
#include <algorithm>

#include "state.h"

class Results: public State {
  CEGUI::Window* menu_window_;

public:
  typedef std::shared_ptr<Results> shared;
  Results(std::shared_ptr<Game> game);
  virtual ~Results();

  void init();
  void update();
  void exit();

 private:
  void add_hooks();
  void add_gui_hooks();
  void add_hooks(const std::string& button,
                 const CEGUI::Event::Subscriber& callback);
  bool change_state(const CEGUI::EventArgs &event);
  void update_menu();
  void switch_menu();

  void insert_record_to_file();
  std::string to_string_with_precision(const float number, const int n);
};


#endif
