// -*- coding: utf-8; mode: c++; tab-width: 4 -*-
#include "game.h"

Menu::Menu(std::shared_ptr<Game> game): State(game) {
}

Menu::~Menu() {
  delete _menu_window;
}

void
Menu::init() {
  init_gui();
  _background = _game->_gui->create_overlay("MenuBackground");
  _background->show();
  get_records();
  add_hooks();
  add_gui_hooks();
}

void
Menu::init_gui() {
  load_windows();
}

void
Menu::load_windows() {
  _menu_window = _game->_gui->load_layout("Menu.layout");

  _record_window = _game->_gui->load_layout("Records.layout");
  _credits_window = _game->_gui->load_layout("Credits.layout");

  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(_menu_window);
}


void
Menu::exit() {
  State::_game->_input->clear_hooks();
  _background->hide();
  State::_game->set_current_state("play");
}

bool
Menu::change_state(const CEGUI::EventArgs &event) {
  exit();
  _menu_window->hide();

  return true;
}

void
Menu::update() {
  _game->_gui->inject_delta(_game->_delta);
}

void
Menu::add_hooks() {
  _game->_input->add_hook({OIS::KC_ESCAPE, true}, EventType::doItOnce,
                          std::bind(&EventListener::shutdown, _game->_input));
}

void
Menu::add_gui_hooks() {
  add_hooks(_menu_window, "Play", CEGUI::Event::Subscriber(&Menu::change_state, this));
  add_hooks(_menu_window, "Records", CEGUI::Event::Subscriber(&Menu::switch_records, this));
  add_hooks(_menu_window, "Credits", CEGUI::Event::Subscriber(&Menu::switch_credits, this));
  add_hooks(_menu_window, "Exit", CEGUI::Event::Subscriber(&EventListener::gui_shutdown,
                                                           _game->_input.get()));
  add_hooks(_record_window, "Back", CEGUI::Event::Subscriber(&Menu::switch_records, this));
  add_hooks(_credits_window, "Back", CEGUI::Event::Subscriber(&Menu::switch_credits, this));
}

bool
Menu::switch_records(const CEGUI::EventArgs &event) {
  if(_menu_window->isActive()){
    _menu_window->hide();
    CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
    context_.setRootWindow(_record_window);
    return true;
  }

  _menu_window->show();
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(_menu_window);
  return true;
}

bool
Menu::switch_credits(const CEGUI::EventArgs &event) {
  if(_menu_window->isActive()){
    _menu_window->hide();
    CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
    context_.setRootWindow(_credits_window);
    return true;
  }

  _menu_window->show();
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(_menu_window);
  return true;
}

void
Menu::add_hooks(CEGUI::Window *window,
                const std::string& button,
                 const CEGUI::Event::Subscriber& callback) {
  window->getChild(button)->
    subscribeEvent(CEGUI::PushButton::EventClicked, callback);
}

void
Menu::get_records() {
  FileManager file_manager;
  std::stringstream element;
  std::vector<std::string> content =
    file_manager.get_content("config/records.txt");
  for (int i = 1; i <= 5; ++i) {
    element << i << "Record";
    _record_window->getChild(element.str())->
      setText(content[i-1]);
    element.str("");
  }
}
