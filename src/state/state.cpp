#include "state.h"
#include "game.h"


State::State(Game::shared game):  _game(game) {

}

State::~State() {

}

void
State::click_button() {
  _clicked_button = get_clicked_node();
  if(_clicked_button != "none")
    _button_triggers[_clicked_button]();
}

std::string
State::get_clicked_node() {
  float mouse_x = _game->_input->_x;
  float mouse_y = _game->_input->_y;
  _game->_scene->set_ray_query(mouse_x, mouse_y);
  Ogre::RaySceneQueryResult &result =
    _game->_scene->_ray_query->execute();

  std::string name;
  for(auto entry: result) {
    name = entry.movable->getParentSceneNode()->getName();
    if(_button_triggers[name]) {
      _clicked_button = name;
      return name;
    }
  }
  return "none";
}

void
State::add_hook(State::Node name, std::function<void()> callback) {
  if(!_button_triggers[name])
    _button_triggers[name] = callback;
}

