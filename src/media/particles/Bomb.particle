particle_system Bomb/WickEmitter
{
	quota	200
	material Bomb/WickEmitterMaterial
	particle_width	1
	particle_height	1
	cull_each	false
	renderer billboard
	sorted false
	local_space	false
	billboard_type point
	billboard_origin center
	billboard_rotation_type	texcoord
	common_up_vector 0 1 0
	point_rendering	true
	accurate_facing	false

	emitter Point
	{
		angle	45
		colour 1 0 0 1
		colour_range_start 1 0 0 1
		colour_range_end 1 0.5 0 1
		direction	0 1 0
		emission_rate	100
		position 0 8 0
		velocity 1
		velocity_min 1
		velocity_max 2
		time_to_live 2.0
		time_to_live_min 2.0
		time_to_live_max 5.0
		duration 2.0
		duration_min 2.0
		duration_max 5.0
		repeat_delay 0
		repeat_delay_min 0
		repeat_delay_max 0
	}

	affector ColourFader
	{
		red	-0.4
		green	-0.4
		blue -0.1
		alpha	-0.5
	}
}

particle_system Bomb/Explosion
{
	material Bomb/ExplosionMaterial
	particle_width 1
	particle_height 1
	quota 50
	cull_each false
	sorted false
	local_space false
	iteration_interval 0.0
	nonvisible_update_timeout 0.0
	billboard_type point
	billboard_origin center
	billboard_rotation_type vertex
	point_rendering false
	accurate_facing false

	emitter Box
	{
		position 0.0 0.0 0.0
		direction 0.0 0.0 1.0
		angle 180
		width 2
		height 2
		depth 2
		emission_rate 50
		velocity_min 0.5
		velocity_max 1.5
		time_to_live_min 1
		time_to_live_max 1
		duration_min 1
		duration_max 1
		repeat_delay_min 0
		repeat_delay_max 0
		colour_range_start 0 0 0 1
		colour_range_end 0 0 0 1
	}

	affector ColourInterpolator
	{
		time0 0
		colour0 0 0 0 1
		time1 0.2
		colour1 1 0.964706 0.556863 1
		time2 0.4
		colour2 0.768627 0.184314 0.0470588 1
		time3 0.6
		colour3 1 0.662745 0.188235 1
		time4 0.8
		colour4 0.780392 0.745098 0.6 1
		time5 1
		colour5 0 0 0 1
	}

	affector Rotator
	{
		rotation_speed_range_start 10
		rotation_speed_range_end 100
		rotation_range_start 3.38
		rotation_range_end 0
	}
}

particle_system Bomb/ObjectExplosion
{
	material Bomb/ExplosionMaterial
	particle_width 1
	particle_height 1
	quota 50
	cull_each false
	sorted false
	local_space false
	iteration_interval 0.0
	nonvisible_update_timeout 0.0
	billboard_type point
	billboard_origin center
	billboard_rotation_type vertex
	point_rendering false
	accurate_facing false

	emitter Box
	{
		position 0.0 0.0 0.0
		direction 0.0 0.0 1.0
		angle 180
		width 2
		height 2
		depth 2
		emission_rate 150.67
		velocity_min 0.5
		velocity_max 1.0
		time_to_live_min 1.5
		time_to_live_max 1.5
		duration_min 1.5
		duration_max 1.5
		repeat_delay_min 0
		repeat_delay_max 0
    colour 1 0 0 1
		colour_range_start 1 0 0 1
		colour_range_end 1 0.5 0 1
	}

  affector ColourFader
	{
		red	-0.4
		green	-0.4
		blue -0.1
		alpha	-0.5
	}

	affector Rotator
	{
		rotation_speed_range_start 10
		rotation_speed_range_end 100
		rotation_range_start 3.38
		rotation_range_end 0
	}
}

particle_system Bomb/Smoke
{
	material Bomb/SmokeMaterial
	particle_width 1
	particle_height 1
	cull_each false
	quota 50
	billboard_type point
	sorted false

	emitter Point
	{
		position 0 -1 0
		angle 15
		emission_rate 25
		time_to_live 3
		direction 0 1 0
		velocity_min 1
		velocity_max 3
		colour 0 0 0 0.4
	}

  affector ColourFader
  {
    red 0
    green 0
    blue 0
    alpha -0.3
  }

  affector Rotator
  {
		rotation_range_start 0
		rotation_range_end 360
		rotation_speed_range_start -60
		rotation_speed_range_end 200
  }
}

particle_system Bomb/ObjectSmoke
{
	material Bomb/SmokeMaterial
	particle_width 1
	particle_height 1
	cull_each false
	quota 50
	billboard_type point
	sorted false

	emitter Point
	{
		position 0 -1 0
		angle 15
		emission_rate 25
		time_to_live 3.5
		direction 0 1 0
		velocity_min 1
		velocity_max 3
		colour 1 1 1 0.4
	}

  affector ColourFader
  {
    red 1
    green 1
    blue 1
    alpha -0.3
  }

  affector Rotator
  {
		rotation_range_start 0
		rotation_range_end 360
		rotation_speed_range_start -60
		rotation_speed_range_end 200
  }
}

particle_system Bomb/ProjSmoke
{
	material Bomb/SmokeMaterial
	particle_width 0.5
	particle_height 0.5
	cull_each false
	quota 25
	billboard_type point
	sorted false

	emitter Point
	{
		position 0 0 0
		angle 180
		emission_rate 10
		time_to_live 1
		direction 0 0 0
		velocity_min 1
		velocity_max 3
		colour 1 1 1 0.4
	}

  affector ColourFader
  {
    red 1
    green 0.88
    blue 0
    alpha -0.3
  }

  affector Rotator
  {
		rotation_range_start 0
		rotation_range_end 360
		rotation_speed_range_start -60
		rotation_speed_range_end 200
  }
}
