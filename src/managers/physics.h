#ifndef PHYSICS_H
#define PHYSICS_H
#include "motionstate.h"
#include "meshstrider.h"
#include <OgreSceneNode.h>
#include <btBulletDynamicsCommon.h>

class Physics {
 public:
  typedef std::pair<const btCollisionObject*, const btCollisionObject*> CollisionPair;
  typedef std::map<CollisionPair, std::function<void()>> Triggers;
  typedef std::shared_ptr<Physics> shared;
  btDiscreteDynamicsWorld* dynamics_world_;

  Physics();
  virtual ~Physics();
  btRigidBody* create_rigid_body(const btTransform &world_transform,
                                 Ogre::SceneNode* node,
                                 btCollisionShape* shape,
                                 btScalar mass, bool active = true);
  btRigidBody* create_rigid_body(btCollisionShape* shape, btVector3 origin, Ogre::SceneNode* node);

  void remove_rigid_body(btRigidBody* body);

  btCollisionShape* create_shape(btVector3 halfExtent);
  btCollisionShape* create_shape(float radius);
  btCollisionShape* create_shape(MeshStrider* strider);
  btCollisionShape* create_shape(btVector3 coordinates, btScalar distance_to_origin);

  btCompoundShape* create_compound_shape(btVector3 origin, btCollisionShape* child);

  void step_simulation(float deltaT, int maxSubSteps);

  void check_collision();

  void clear_triggers();
  void add_collision_hooks(CollisionPair key, std::function<void()> callback);

 void set_linear_velocity(btRigidBody* body, btVector3 velocity);
 void move(btRigidBody* body, btVector3 position);
 private:
  const btVector3 gravity_ = btVector3(0, 0, 0);

  btBroadphaseInterface* broadphase_;
  btSequentialImpulseConstraintSolver* solver_;
  btDefaultCollisionConfiguration* collision_configuration_;
  btCollisionDispatcher* dispatcher_;

  Triggers triggers_;
};

#endif
