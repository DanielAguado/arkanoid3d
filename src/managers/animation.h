#ifndef ANIMATION_H
#define ANIMATION_H

#include <memory>
#include <OgreAnimationState.h>
#include <OgreEntity.h>
#include <map>
#include <algorithm>

class Animation {
public:
	typedef std::shared_ptr<Animation> shared;
  typedef std::function<void()> Callback;

	Animation();
	~Animation();

	void activate(Ogre::Entity* entity, std::string animation_name, 
			Animation::Callback callback);

	void update(float deltaT);

private:
	std::map<Ogre::AnimationState*, Callback> _active_animations;
};

#endif
