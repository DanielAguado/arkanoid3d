#include "physics.h"

Physics::Physics() {
    btVector3 world_min(-1000,-1000,-1000);
    btVector3 world_max(1000,1000,1000);

    broadphase_ = new btAxisSweep3(world_min,world_max);

    solver_ = new btSequentialImpulseConstraintSolver();
    collision_configuration_ = new btDefaultCollisionConfiguration();
    dispatcher_ = new btCollisionDispatcher(collision_configuration_);

    dynamics_world_ = new btDiscreteDynamicsWorld(dispatcher_,
                        broadphase_, solver_, collision_configuration_);
    dynamics_world_->setGravity(gravity_);
  }



Physics::~Physics() {
  delete dynamics_world_;
  delete dispatcher_;
  delete collision_configuration_;
  delete solver_;
  delete broadphase_;
}

btRigidBody*
Physics::create_rigid_body(const btTransform &world_transform,
                  Ogre::SceneNode* node,
                  btCollisionShape* shape,
                  btScalar mass, bool active){
  btVector3 inertia(0 ,0 ,0);

  if(mass != 0)
    shape->calculateLocalInertia(mass, inertia);

  MotionState* motionState = new MotionState(world_transform, node);
  btRigidBody::btRigidBodyConstructionInfo
    rigidBodyCI(mass, motionState, shape, inertia);

  btRigidBody* rigidBody = new btRigidBody(rigidBodyCI);
  dynamics_world_->addRigidBody(rigidBody);

  if(active)
    rigidBody->setActivationState(DISABLE_DEACTIVATION);

  return rigidBody;
}

void
Physics::remove_rigid_body(btRigidBody* body) {
  dynamics_world_->removeRigidBody(body);
}

btCollisionShape*
Physics::create_shape(btVector3 halfExtent){
  return new btBoxShape(halfExtent);
}

btCollisionShape*
Physics::create_shape(float radius){
  return new btSphereShape(radius);
}

btCollisionShape*
Physics::create_shape(MeshStrider* strider){
  return new btBvhTriangleMeshShape(strider, true, true);
}

btCollisionShape*
Physics::create_shape(btVector3 coordinates,
                      btScalar distance_to_origin) {
  return new btStaticPlaneShape(coordinates, distance_to_origin);
}

btCompoundShape*
Physics::create_compound_shape(btVector3 origin, btCollisionShape* child){
  btCompoundShape* compound = new btCompoundShape();
  btTransform localTrans;
  localTrans.setIdentity();
  localTrans.setOrigin(origin);

  compound->addChildShape(localTrans, child);
  return compound;
}

btRigidBody*
Physics::create_rigid_body(btCollisionShape* shape, btVector3 origin, Ogre::SceneNode* node) {
  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btTransform transformation(rotation, origin);

  return create_rigid_body(transformation, node, shape, 0, false);
}

void
Physics::step_simulation(float deltaT, int maxSubSteps) {
  dynamics_world_->stepSimulation(deltaT, maxSubSteps);
}

void
Physics::check_collision() {
  int contact_point_caches = dynamics_world_->getDispatcher()->getNumManifolds();
  for (int i = 0; i < contact_point_caches; i++) {
      btPersistentManifold* contact_cache =
        dynamics_world_->getDispatcher()->getManifoldByIndexInternal(i);

      const btCollisionObject* object_a = contact_cache->getBody0();
      const btCollisionObject* object_b = contact_cache->getBody1();

      if(triggers_[Physics::CollisionPair{object_a, object_b}]) {
        triggers_[Physics::CollisionPair{object_a, object_b}]();
      }
      if (triggers_[Physics::CollisionPair{object_b, object_a}]) {
        triggers_[Physics::CollisionPair{object_b, object_a}]();
      }
    }
}


void
Physics::add_collision_hooks(Physics::CollisionPair collision_pair, std::function<void()> callback)  {
  if(!triggers_[collision_pair]) {
    triggers_[collision_pair] = callback;
  }
}

void
Physics::clear_triggers() {
  triggers_.clear();
}


void
Physics::move(btRigidBody* body, btVector3 position) {
  btTransform transform = body->getCenterOfMassTransform();
  transform.setOrigin(position);
  body->setCenterOfMassTransform(transform);
}

void
Physics::set_linear_velocity(btRigidBody* body,
                             btVector3 velocity) {
  body->setLinearVelocity(velocity);
}
